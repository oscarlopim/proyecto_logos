from enum import Enum
from keras.layers import ELU, LeakyReLU

def get_my_relu(relu_type):
    leaky_value = 0.15
    if relu_type == ModelCnst.USE_ELU:
        x = ELU()
    if relu_type == ModelCnst.USE_LEAKY:
        x = LeakyReLU(alpha=leaky_value)
    return x

class ModelCnst(Enum):
    VAE_MLP = 0
    VAE_MLP_VARIANCE = 1
    VAE_MLP_PEEP = 2
    USE_ELU = 3
    USE_LEAKY = 4

class ModelP():
    def __init__(self):
        # default
        self.vae_type = ModelCnst.VAE_MLP
        self.relu_type = ModelCnst.USE_LEAKY
        self.input_shape = 228
        self.show_plot = 1
        self.show_summay = 1
        self.hidden_act_fun = 'relu'
        self.enc_hidden_units = 10
        self.dec_hidden_units = 10
        self.latent_dim = 2
        self.rec_factor = 1
        self.kl_factor = 1
        self.dropout = 0
        
        self.dic = ModelCnst._member_names_
    def print(self):
        print("-"*50)
        print("Model parameters:")
        print("\t vae_type:",self.vae_type.name)
        print("\t relu_type:",self.relu_type.name)
        print("\t input_shape:",self.input_shape)
        print("\t show_plot:",self.show_plot)
        print("\t show_summay:",self.show_summay)
        print("\t hidden_act_fun:",self.hidden_act_fun)
        print("\t enc_hidden_units:",self.enc_hidden_units)
        print("\t dec_hidden_units:",self.dec_hidden_units)
        print("\t latent_dim:",self.latent_dim)
        print("\t rec_factor:",self.rec_factor)
        print("\t kl_factor:",self.kl_factor)
        print("\t dropout:",self.dropout)
        print("-"*50)
    
        