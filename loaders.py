import numpy as np
from os import walk
import imageio
import scipy
from scipy.ndimage import rotate
from scipy.misc import imresize
from progressBar import ProgressBar

def read_image(filename):
	return imageio.imread(filename)

def scale_image(img, x_scale, y_scale):
	new_x = img.shape[0]*x_scale
	new_y = img.shape[1]*y_scale
	#print(img.shape)
	#print((new_x,new_y))
	return imresize(img, (int(new_x),int(new_y)))

def rotate_image(img, angle, cval):
	return rotate(img, angle, reshape=False, cval = cval)

def flipped_horizontal(img):
	return np.copy(np.flip(img, axis=1))

def flipped_vertical(img):
	return np.copy(np.flip(img, axis=0))

def fill_expand(img, max_width, max_height):
	new_img = np.ones((int(max_width), int(max_height),3))*255
	#print(new_img.shape)
	mid_y = int(max_width/2)
	mid_x = int(max_height/2)
	img_width = int(img.shape[1]/2)
	img_height = int(img.shape[0]/2)
	x_rest = img.shape[1]%2
	y_rest = img.shape[0]%2
	new_img[mid_y-img_height:mid_y+img_height+y_rest,mid_x-img_width:mid_x+img_width+x_rest,:] = img[:,:,:3]
	return new_img.astype(int)

def displace(img, dx=0, dy=0, white_value=255):
	new_img = np.copy(img)
	prop = 0.9
	max_abs_dx = int(new_img.shape[1]*prop)
	max_abs_dy = int(new_img.shape[0]*prop)
	dx = np.clip(dx,-max_abs_dx,max_abs_dx)
	dy = np.clip(dy,-max_abs_dy,max_abs_dy)
	pix = white_value
	if dx>0:
		new_img[:,dx:,:] = new_img[:,:-dx,:]
		new_img[:,:dx,:] = pix
	elif dx<0:
		new_img[:,:dx,:] = new_img[:,-dx:,:]
		new_img[:,dx:,:] = pix
		
	if dy>0:
		new_img[dy:,:,:] = new_img[:-dy,:,:]
		new_img[:dy,:,:] = pix
	elif dy<0:
		new_img[:dy,:,:] = new_img[-dy:,:,:]
		new_img[dy:,:,:] = pix
	return new_img

class LogoLoader(object):
	def __init__(self, folder_name):
		self.names = []
		self.extentions = []
		self.dataset = {}
		self.folder_name = folder_name
		for (dirpath, dirnames, filenames) in walk(self.folder_name):
			for filename in filenames:
				self.names.append(filename[:-4])
				self.extentions.append(filename[-3:])
			break
		self.size = len(self.names)
		self.widths = np.zeros(self.size )
		self.heights = np.zeros(self.size )

		for i in range(self.size):
			name = self.names[i]
			extention = self.extentions[i]
			img = imageio.imread(self.folder_name+'/'+name+'.'+extention)
			self.widths[i] = img.shape[1] # transpose?
			self.heights[i] = img.shape[0]
			self.dataset[name] = {} # generate empty list (original, piracy)
			self.dataset[name]['ext'] = extention # of file

		self.max_width = np.array([self.heights.max(),self.heights.max()]).max()
		self.max_height = np.array([self.widths.max(),self.widths.max()]).max()

		self.print_max_image_size()
		self.print_dataset_names()

	def generate_data_augmentation(self, n_data_agumentation, scale):
		progress = ProgressBar(self.size, fmt=ProgressBar.FULL)
		
		max_dim = max(self.max_width, self.max_height)
		print("Data augmentation:")
		for name in self.get_keys():
			progress(name)
			filename = self.folder_name+'/'+name+'.'+self.dataset[name]['ext']
			img_original = read_image(filename)
			
			img_original = fill_expand(img_original, max_dim, max_dim)
			#img = fill_expand(img, logoLoader.max_width, logoLoader.max_height)
			img_original = scale_image(img_original, scale, scale)
			#img = np.flip(img, axis=1)
			self.dataset[name]['original'] = img_original # original
			self.dataset[name]['da'] = []
			#self.dataset[name]['da'].append(img) # piracy
			for is_flip_x in [False,True]:
				for is_flip_y in [False,True]:
					for i in range(n_data_agumentation):
						#print(is_flip_x,is_flip_y)
						if (is_flip_x and is_flip_y):
							img = flipped_horizontal(flipped_vertical(img_original))
						elif (is_flip_x and not is_flip_y):
							img = flipped_horizontal(img_original)
						elif (not is_flip_x and is_flip_y):
							img = flipped_vertical(img_original)
						else:
							img = img_original
						angle = i/n_data_agumentation*360.0
						img = rotate_image(img, angle, 255)
						self.dataset[name]['da'].append(img) # piracy
		progress.done()

	def load_plagios(self, deside_dim = 132):
		dataset = {}
		progress = ProgressBar(self.size, fmt=ProgressBar.FULL)
		max_dim = max(self.max_width, self.max_height)
		print("Loading plagios:")
		for name_id in self.get_keys():
			progress(name_id)
			filename = self.folder_name+'/'+name_id+'.'+self.dataset[name_id]['ext']
			name = ''.join([i for i in name_id if not i.isdigit()])
			img_id = int(''.join(filter(str.isdigit, name_id)))
			#print('name:',name)
			#print(img_id)
			img = read_image(filename)
			img = fill_expand(img, max_dim, max_dim)
			#img = fill_expand(img, logoLoader.max_width, logoLoader.max_height)
			img = imresize(img, (int(deside_dim),int(deside_dim)))
			#img = np.flip(img, axis=1)
			if not name in dataset:
				dataset[name] = {}
				dataset[name]['plagio'] = []
			else: # ERROR ACA... solucionable en alguna vida siguiente... :( solo hay logos que no son considerados -> if True: pero tendria que hacer todos los experimentos de nuevo, ademas, falta un append del original en los plagios.... lo lamento
				if img_id==0:
					dataset[name]['original'] = img # original
				else:
					dataset[name]['plagio'].append(img)
					dataset[name]['plagio'].append(flipped_horizontal(img))
		progress.done()
		self.dataset = dataset


	def print_max_image_size(self):
		print("max width: {} (idx {})".format(self.widths.max(),self.widths.argmax()))
		print("max height: {} (idx {})".format(self.heights.max(),self.heights.argmax()))

	def print_dataset_names(self):
		print("dataset size:",len(self.dataset.keys()))
		print('-'*60)
		for name in self.dataset.keys():
			print(name, end=', ')
		print("\n")

	def get_keys(self):
		return self.dataset.keys()

	def total_samples(self):
		samples = 0
		for key in self.dataset.keys():
			samples += len(self.dataset[key]['da'])
		return samples 

######################################################################################
######################################################################################


class PairDatasetLoader(object):
	def __init__(self, list_genuine, list_impostor, batch_size):
		self.list_genuine = list_genuine
		self.list_impostor = list_impostor
		self.batch_size = batch_size
		self.dim = list_genuine[0][0].shape

	def generate(self):
		while True:
			n = self.batch_size
			targets = np.ones((n,)) # 1 1 1 1 0 0 0 0
			targets[n//2:] = 0 # same class for first half
			pairs = [np.zeros((n, self.dim[0],self.dim[1],self.dim[2])) for i in range(2)]
			MAX_DISP = 5 # max
			for i in range(n):
				#pick images of same class for 1st half, different for 2nd
				if i <= n//2:
					idx = np.random.randint(0,len(self.list_genuine))
					pairs[0][i,:,:,:] = self.list_genuine[idx][0][:,:,:]/255
					pairs[1][i,:,:,:] = displace(self.list_genuine[idx][1][:,:,:],MAX_DISP,MAX_DISP)/255
				else:
					idx = np.random.randint(0,len(self.list_impostor))
					pairs[0][i,:,:,:] = self.list_impostor[idx][0][:,:,:]/255
					pairs[1][i,:,:,:] = displace(self.list_impostor[idx][1][:,:,:],MAX_DISP,MAX_DISP)/255
					
			yield (pairs, targets)