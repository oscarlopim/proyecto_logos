
from __future__ import print_function
import sys
import re

class ProgressBar(object):
	DEFAULT = 'Progress: %(bar)s %(percent)3d%%'
	FULL = '%(current)d/%(total)d %(bar)s %(percent)3d%% - %(remaining)d remaining - %(string)s'

	def __init__(self, total, width=40, fmt=DEFAULT, symbol='=',output=sys.stderr):
		assert len(symbol) == 1
		self.total = total
		self.width = width
		self.symbol = symbol
		self.output = output
		self.fmt = re.sub(r'(?P<name>%\(.+?\))d',r'\g<name>%dd' % len(str(total)), fmt)
		self.current = 0
		self.done_ = False
		self.last_char = ''
		self.last_name = ''

	def __call__(self, name=''):
		percent = self.current / float(self.total)
		size = int(self.width * percent)
		if(self.done_):
			last_char = self.symbol
		else:
			last_char = '>'
			if(size==0):
				last_char = ''
			self.current += 1
		remaining = self.total - self.current
		bar = '[' + self.symbol * (size-1)+ last_char + ' ' * (self.width - size) + ']'
		args = {
			'total': self.total,
			'bar': bar,
			'current': self.current,
			'percent': percent * 100,
			'remaining': remaining,
			'string': name+' '*(len(self.last_name)-len(name))
		}
		print('\r' + self.fmt % args, end='')
		self.last_name = name

	def done(self):
		self.current = self.total
		self.done_ = True
		self(self.last_name)
		print('')
